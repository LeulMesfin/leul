# The Price Paid
What strikes me the most about Steven Shaviro's, *Connected*, is the conceptual idea he discusses about how every connection has a price to be paid.
As Shaviro was discussing connections, he says, 
> "Every Connection has its price; the one thing you can be sure of is that, sooner or later, you will have to pay"
(Shaviro 3).

Shaviro believes that society seeks to have everyone connected with out truly realizing what having everyone connected would truly mean and the price that each connection came with that must be paid. In society, we are connected through many different ways other than the typical one you might think like as all being connected through the web. 

Markdown is a conversion software that allows you to see the underlying code behind the text of what you are writing in a clear and understandable manner for someone who is inexperienced in web coding. It allows the writer to connect to the software.
With markdown, the cost is you are forced to choose and lead what you want to not give attention towards such as certain indicators such as words that are ~~crossed out~~ like that and choose what you want to be more emphasized such as something being **Bolded** regardless of its ```true significance.```
The difference between using markdown is that you have to code what you want to be emphasised or not compared to other textual softwares that code it for you.
For example, look at powerpoint presentations that a teacher provides you to convey information about a certain topic during a lecture. When taking notes you are lead to believe that the most important and relevant information is the words that are ***bolded***.
In reality, all the words could have the same importance but certain textual indicators want us to believe some words are more important than others. 
Additionally to that, you are able to distinguish what the 
# title is, 
what the 
#### header is, 
and what the text is all by the simple size of the text.


Connection comes in all different types of forms. Like in the United States, one thing that we are all connected through is the language of English. That Connection has a price in the fact that it controls the way we understand things. We are inherently forced to think in English, dream in English, and write in English by knowing the language. 
When you try to learn other languages other than English, you are forced to translate that language into its English meaning and correlate the words. You are forced to understand things to the scope of how far the English language goes and essentially are constrained to the current advancements of the language. 
There are countless different ways that the languge of English connects us and countless prices we have to pay for all the different various ways it connects us and that is what is truly fascinating about it all.